public class MergeSort{
	public static void sort(int[] values){
		mergeSort(values, 0, values.length-1);
	}

	private static void mergeSort(int[] values, int left, int right){
		if (left < right){
			int middle = (left+right)/2;
			mergeSort(values, left, middle);
			mergeSort(values, middle+1, right);
			merge(values, left, middle, right);
		}
	}

	private static void merge(int[] values, int left, int middle, int right){
		int[] helper = new int[values.length];
		for (int i = left; i <= right; i++){
			helper[i] = values[i];
		}

		int i = left;
		int j = middle+1;
		int k = left;

		while ((i <= middle) && (j <= right)){
			if (helper[i] <= helper[j]){
				values[k] = helper[i];
				i++;
			}
			else{
				values[k] = helper[j];
				j++;
			}
			k++;
		}
		
		while (i <= middle){
			values[k] = helper[i];
			i++;
			k++;
		}

	}
}
