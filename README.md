A Java implementation of MergeSort.

Takes an array of primitive integers, and sorts it recursively using MergeSort.
